

bill_total = float(input('Please enter the bill total: '))
num_guests = int(input('Please enter the number of guests. '))
tip = float(.15)
tip_total = float(bill_total * tip)
split_bill = float(bill_total / num_guests)
split_tip = float(tip_total / num_guests)
split_total = float(split_bill + split_tip)
split = round(split_total, 2)

print(f"Each person's total is ${split}.")
